package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class App {
    public static Map<String, Integer> getWordCount(String text) {
        
        HashMap<String, Integer> result = new HashMap<>();

        if ("".equals(text)) {
            return result;
        }

        String[] words = text.split(" ");

        for (String word: words) {
            Integer count = result.get(word);
            if (count == null) {
                count = 0;
            }
            count++;
            result.put(word, count);
        }

        return result;
    }

    public static String toString(Map<String, Integer> map) {

        StringBuilder result = new StringBuilder("{");

        for (Map.Entry<String, Integer> entry: map.entrySet()) {
            result.append("\n  ");
            result.append(entry.getKey());
            result.append(": ");
            result.append(entry.getValue());
        }

        if (map.size() > 0) {
            result.append("\n");
        }

        result.append("}");
        return result.toString();
    }
}
//END

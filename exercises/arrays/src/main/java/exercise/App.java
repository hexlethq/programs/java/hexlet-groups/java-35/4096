package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] arr) {
        if (arr == null) return new int[0];
        int numberOfElements = arr.length;
        int[] newArr = new int[numberOfElements];
        int c = 0;
        while (c < numberOfElements) {
            newArr[c] = arr[numberOfElements - c - 1];
            c++;
        }
        return newArr;
    }

    public static int mult(int[] arr) {
        if (arr == null) return 0;
        int result = 1;
        for (int el: arr) {
            result *= el;
        }
        return result;
    }
    // END
}

package exercise;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        int length = sentence.length();
        if (sentence.charAt(length-1) == '!')
            System.out.println(sentence.toUpperCase());
        else
            System.out.println(sentence.toLowerCase());
        // END
    }
}

package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        int i = 0;
        String starsString = "";
        while (i++ <starsCount )
            starsString += "*";
        System.out.println(starsString + cardNumber.substring(cardNumber.length() - 4));
        // END
    }
}

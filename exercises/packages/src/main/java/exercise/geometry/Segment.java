package exercise.geometry;

// BEGIN
public class Segment {
    private double[] beginPoint;
    private double[] endPoint;
//    makeSegment() — принимает на вход две точки и возвращает отрезок.
//    getBeginPoint() — принимает на вход отрезок и возвращает точку начала отрезка.
//            getEndPoint()
    public static Segment makeSegment(double[] beginPoint, double[] endPoint) {
        Segment segment = new Segment();
        segment.beginPoint = beginPoint;
        segment.endPoint = endPoint;
        return segment;
    }

    public static double[] getBeginPoint(Segment segment) {
        return segment.beginPoint;
    }

    public static double[] getEndPoint(Segment segment) {
        return segment.endPoint;
    }
}
// END

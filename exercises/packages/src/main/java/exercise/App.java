// BEGIN
package exercise;

import exercise.geometry.Point;
import exercise.geometry.Segment;

public class App {
    public static double[] getMidpointOfSegment(Segment segment) {
        double[] beginPoint = Segment.getBeginPoint(segment);
        double[] endPoint = Segment.getEndPoint(segment);
        double x = (beginPoint[0] + endPoint[0]) / 2;
        double y = (beginPoint[1] + endPoint[1]) / 2;
        double[] newPoint = new double[2];
        newPoint[0] = x;
        newPoint[1] = y;
        return newPoint;
    }

    public static Segment reverse(Segment segment) {
        double[] newBeginPoint = Segment.getEndPoint(segment).clone();
        double[] newEndPoint = Segment.getBeginPoint(segment).clone();
        return Segment.makeSegment(newBeginPoint, newEndPoint);
    }

    public static boolean isBelongToOneQuadrant(Segment segment) {
        int beginQuadrant = getQuadrant(Segment.getBeginPoint(segment));
        int endQuadrant   = getQuadrant(Segment.getEndPoint(segment));
        if (beginQuadrant == 0 || endQuadrant == 0) {
            return false;
        }
        return beginQuadrant == endQuadrant;
    }

    public static int getQuadrant(double[] point) {
        double x = point[0];
        double y = point[1];
        if (x == 0 || y == 0) {
            return 0;
        }
        if (x > 0 && y > 0) {
            return 1;
        } else if (x > 0 && y < 0) {
            return 4;
        } else if (x < 0 && y > 0) {
            return 2;
        } else {
            return 3;
        }
    }
}


// END

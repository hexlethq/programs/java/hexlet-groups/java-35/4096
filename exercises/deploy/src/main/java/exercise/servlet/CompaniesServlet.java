package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.stream.Collectors;
import static exercise.Data.getCompanies;

public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        // BEGIN
        String searchName = request.getParameter("search");
        if (searchName == null) {
            searchName = "";
        }

        List<String> companies = getCompanies();
        String finalSearchName = searchName;
        List<String> fileredCompanies = companies.stream().filter((x) -> (x != null && x.contains(finalSearchName))).toList();
        PrintWriter out = response.getWriter();

        if (fileredCompanies.isEmpty()) {
            out.println("Companies not found");
        }
        for (String company: fileredCompanies) {
            out.println(company);
        }
        out.close();
        // END
    }
}

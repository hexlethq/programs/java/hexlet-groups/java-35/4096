package exercise;

import java.util.Map;
import java.util.HashMap;

// BEGIN
public class InMemoryKV implements KeyValueStorage{

    Map<String, String> kv;

    public InMemoryKV(Map<String, String> map) {
        kv = new HashMap<>(map);
    }

    @Override
    public void set(String key, String value) {
        kv.put(key, value);
    }

    @Override
    public void unset(String key) {
        kv.remove(key);
    }

    @Override
    public String get(String key, String defaultValue) {
        return kv.getOrDefault(key, defaultValue);
    }

    @Override
    public Map<String, String> toMap() {
        return new HashMap<String, String>(kv);
    }

}
// END

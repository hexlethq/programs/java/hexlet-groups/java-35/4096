package exercise;

import java.util.Map;

// BEGIN
public class FileKV implements KeyValueStorage {

    String fileName;

    public FileKV(String fileName, Map<String, String> kv) {
        this.fileName = fileName;
        writeMapFromFile(kv);
    }

    @Override
    public void set(String key, String value) {
        Map<String, String> kv = loadMapFromFile();
        kv.put(key, value);
        writeMapFromFile(kv);
    }

    @Override
    public void unset(String key) {
        Map<String, String> kv = loadMapFromFile();
        kv.remove(key);
        writeMapFromFile(kv);
    }

    @Override
    public String get(String key, String defaultValue) {
        Map<String, String> kv = loadMapFromFile();
        return kv.getOrDefault(key, defaultValue);
    }

    @Override
    public Map<String, String> toMap() {
        return loadMapFromFile();
    }

    private Map<String, String> loadMapFromFile() {
        return Utils.unserialize(Utils.readFile(fileName));
    }

    private void writeMapFromFile(Map<String, String> kv) {
        Utils.writeFile(fileName, Utils.serialize(kv));
    }
}
// END

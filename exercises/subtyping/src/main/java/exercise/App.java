package exercise;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

// BEGIN
public class App {
    public static void swapKeyValue(KeyValueStorage kv) {

        kv.toMap().forEach((key, value) -> {
            kv.unset(key);
            kv.set(value, key);
        });

    }
}
// END

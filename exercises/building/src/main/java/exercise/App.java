// BEGIN
package exercise;

import com.google.gson.Gson;

class App {
    public static void main(String[] args) {
        System.out.println("Hello, World!");
    }
    public static String toJson(String[] array) {
        return new Gson().toJson(array);
    }
}
// END

package exercise;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;


// BEGIN
public class Kennel {
    private static ArrayList<String[]> puppies = new ArrayList<String[]>();
    public static void addPuppy(String[] puppy) {
        if (puppy == null) return;
        puppies.add(puppy);
    }

    public static void addSomePuppies(String[][] newPuppies) {
        if (newPuppies == null) return;
        for (String[] puppy: newPuppies) {
            puppies.add(puppy);
        }
    }

    public static int getPuppyCount() {
        return puppies.size();
    }

    public static boolean isContainPuppy(String name) {
        if (name == null) return false;
        for (String[] puppy: puppies) {
            if (name.equals(puppy[0])) return true;
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        String[][] allPuppies = new String[puppies.size()][2];
        int counter = 0;
        for (String [] puppy: puppies) {
            allPuppies[counter++] = puppy;
        }
        return allPuppies;
    }

    public static String[] getNamesByBreed(String breed) {
        if (breed == null) return new String[0];
        int counter = 0;
        for (String [] puppy: puppies) {
            if (breed.equals(puppy[1])) counter++;
        }
        String[] namesByBreed = new String[counter];
        counter = 0;
        for (String [] puppy: puppies) {
            if (breed.equals(puppy[1])) {
                namesByBreed[counter++] = puppy[0];
            }
        }
        return namesByBreed;
    }

    public static void resetKennel() {
        puppies.clear();
    }

    public static boolean removePuppy(String name) {
        if (name == null) return false;
        for (String [] puppy: puppies) {
            if (name.equals(puppy[0])) {
                puppies.remove(puppy);
                return true;
            }
        }
        return false;
    }

}
// END

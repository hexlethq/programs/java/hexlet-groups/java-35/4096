package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;

import java.sql.Statement;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import org.apache.commons.lang3.ArrayUtils;

import exercise.TemplateEngineUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;



public class ArticlesServlet extends HttpServlet {

    private String getId(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return null;
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 1, null);
    }

    private String getAction(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return "list";
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 2, getId(request));
    }

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String action = getAction(request);

        switch (action) {
            case "list":
                showArticles(request, response);
                break;
            default:
                showArticle(request, response);
                break;
        }
    }

    private void showArticles(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException, ServletException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        var articles = new ArrayList<Map<String, String>>();
        try {
            String query;
            String pageParam = request.getParameter("page");
            int page;
            PreparedStatement statement;
            if (pageParam == null) {
                query = "SELECT id, title FROM articles order by id LIMIT ?";
                statement = connection.prepareStatement(query);
                page = 0;
            } else {
                page = Integer.valueOf(pageParam);
                query = "SELECT id, title FROM articles order by id LIMIT ? OFFSET ?";
                statement = connection.prepareStatement(query);
                statement.setInt(2, (page - 1) * 10);
            }
            request.setAttribute("page", page);
            statement.setInt(1, 10);
            ResultSet rs = statement.executeQuery();
             while (rs.next()) {
                 articles.add(
                     Map.of(
                     "id", rs.getString("id"),
                     "title", rs.getString("title")
                     )
                 );
             }

        } catch (SQLException e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }
        request.setAttribute("articles", articles);
        // END
        TemplateEngineUtil.render("articles/index.html", request, response);
    }

    private void showArticle(HttpServletRequest request,
                         HttpServletResponse response)
                 throws IOException, ServletException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        try {
            String id = getId(request);
            String query = "SELECT id, title, body FROM articles where id = " + id;
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet rs = statement.executeQuery();
            String title = "", body = "";
            if (rs.next()) {
                title = rs.getString("title");
                body  = rs.getString("body");
            }
            else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            };
            request.setAttribute("title", title);
            request.setAttribute("body" , body);
        } catch (SQLException e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }
        // END
        TemplateEngineUtil.render("articles/show.html", request, response);
    }
}

package exercise;

class Point {
    // BEGIN
    private int x;
    private int y;
    public static Point makePoint(int x, int y) {
        var point = new Point();
        point.x = x;
        point.y = y;
        return point;
    }

    public static int getX(Point point) {
        return point.x;
    }

    public static int getY(Point point) {
        return point.y;
    }

    public static String pointToString(Point point) {
        return "(" + point.x + ", " + point.y + ")";
    }

    public static int getQuadrant(Point point) {
        if (point.x == 0 || point.y == 0) {
            return 0;
        }
        if (point.x > 0 && point.y > 0) {
            return 1;
        } else if (point.x > 0 && point.y < 0) {
            return 4;
        } else if (point.x < 0 && point.y > 0) {
            return 2;
        } else {
            return 3;
        }
    }
    public static Point getSymmetricalPointByX(Point point) {
        return makePoint(point.x, -point.y);
    }

    public static int calculateDistance(Point point1, Point point2) {
        return (int) Math.sqrt((point2.x - point1.x) * (point2.x - point1.x) + (point2.y - point1.y) * (point2.y - point1.y));
    }
    // END
}

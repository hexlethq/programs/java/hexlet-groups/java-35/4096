package exercise;

class Converter {
    // BEGIN
    public static void main(String[] args) {
        System.out.print("10 Kb = ");
        System.out.print(Converter.convert(10, "b"));
        System.out.println(" b");
    }

    public static int convert(int size, String unitOfMeasurement) {
        if (unitOfMeasurement.equals("b")) {
            return size * 1024;
        } else if (unitOfMeasurement.equals("Kb")) {
            return size / 1024;
        }
        return 0;
    }
    // END
}

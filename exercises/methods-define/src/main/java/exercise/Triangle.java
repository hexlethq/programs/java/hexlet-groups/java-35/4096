package exercise;

class Triangle {
    // BEGIN
    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45));
    }

    public static double getSquare(int firstSide, int secondSide, int angle) {
        return Math.sin(angle * Math.PI / 180) * ((firstSide * secondSide) / 2.0);
    }
    // END
}

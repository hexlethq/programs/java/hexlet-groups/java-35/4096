package exercise;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.jar.Attributes;
import java.util.stream.Collectors;
import java.util.Map;

// BEGIN
public class Tag {
    private String name;
    private Map<String, String> attributes;

    public Tag(String name, Map<String, String> attributes) {
        this.name = name;
        this.attributes = new LinkedHashMap<>(attributes);
    }

    @Override
    public String toString() {
        String attributesString = attributes.entrySet().stream().
                map(x -> x.getKey() + "=\"" + x.getValue() + "\"").
                collect(Collectors.joining(" "));
        if (attributesString.length() > 0) attributesString = " " + attributesString;
        return "<" + name + attributesString + ">";
    }

    public String getName() {
        return name;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }
}
// END

package exercise;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class PairedTag extends Tag{
    private String body;
    private List<Tag> childer;

    public PairedTag(String name, Map<String, String> attributes, String body, List<Tag> childer) {
        super(name, attributes);
        this.body = body;
        this.childer = new ArrayList<Tag>(childer);
    }

    public String getBody() {
        return body;
    }

    @Override
    public String toString() {
        String childerStr = childer.stream().
                map(x -> x.toString()).
                collect(Collectors.joining());
        return super.toString() + getBody() + childerStr + "</" + getName() + ">";
    }
}
// END

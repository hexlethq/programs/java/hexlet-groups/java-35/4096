package exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

// BEGIN
public class App {
     public static boolean scrabble(String inputChars, String inputWord) {

          List<Character> chars = new ArrayList<>();
          for (int i = 0; i < inputChars.length(); i++) {
               chars.add(inputChars.charAt(i));
          }
          String word = String.copyValueOf(inputWord.toCharArray()).toLowerCase(Locale.ROOT);
          for (int i = 0; i < word.length(); i++) {
               Character currentChar = word.charAt(i);
               if (!chars.contains(currentChar))
                    return false;
               chars.remove(currentChar);
          }
          return true;
     }
}
//END
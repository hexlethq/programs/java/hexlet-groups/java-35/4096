package exercise;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

// BEGIN
public class App {
    public static void save(Path file, Car car) throws IOException {
        Files.writeString(file, Car.serialize(car));
    }

    public static Car extract(Path file) throws IOException {
        String json = Files.readString(file);
        return Car.unserialize(json);
    }
}
// END

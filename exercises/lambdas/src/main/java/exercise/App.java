package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static String[][] enlargeArrayImage(String[][] arr) {
       return Arrays.stream(arr)
               .map((x) -> {
                    return Arrays.stream(x)
                            .flatMap(y -> (Stream.of(y, y)))
                            .toArray(String[]::new);
                })
               .flatMap(x -> Stream.of(x, x))
               .toArray(String[][]::new);
    }
}
// END

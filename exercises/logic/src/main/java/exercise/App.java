package exercise;

class App {
    public static boolean isBigOdd(int number) {
        // BEGIN
        boolean isBigOddVariable = false;
        if (number % 2 != 0 && number >= 1001) {
            isBigOddVariable = true;
        }
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        if (number % 2 == 0) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        String quarter = "";
        if (minutes >= 0 && minutes <= 14) {
            quarter = "First";
        }
        else if (minutes >= 15 && minutes <= 29) {
            quarter = "Second";
        }
        else if (minutes >= 30 && minutes <= 44) {
            quarter = "Third";
        }
        else {
            quarter = "Fourth";
        }
        System.out.println(quarter);
        // END
    }
}
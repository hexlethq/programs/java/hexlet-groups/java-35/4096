package exercise.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.net.URL;
import java.util.*;

import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.ArrayUtils;

@WebServlet("/users/*")
public class UsersServlet extends HttpServlet {

    static final String BOOTSTRAP_STRING = "<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3\" crossorigin=\"anonymous\">";

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }
        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        ObjectMapper objectMapper = new ObjectMapper();

        String file = Files.readString(Paths.get("src","main", "resources", "users.json"));
        return objectMapper.readValue(file, new TypeReference<List<Map<String, String>>>(){});
        // END
    }

    private void showUsers(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException {

        // BEGIN
        List<Map<String, String>> users = getUsers();
        StringBuilder table = new StringBuilder("<html><head>"
                + BOOTSTRAP_STRING
                + "</head><body><table border=1>");
        response.setLocale(Locale.ROOT);

        for (Map<String, String> user: users) {
            table.append("<tr border=1><td>");
            table.append(user.get("id"));
            table.append("</td><td><a href=\"/users/");
            table.append(user.get("id"));
            table.append("\">");
            table.append(user.get("firstName"));
            table.append(" ");
            table.append(user.get("lastName"));
            table.append("</a></td></tr>");
        }
        table.append("</table></body></html>");
        PrintWriter pw = response.getWriter();
        pw.write(table.toString());
        pw.close();
        // END
    }

    private void showUser(HttpServletRequest request,
                         HttpServletResponse response,
                         String id)
                 throws IOException {

        // BEGIN
        List<Map<String, String>> users = getUsers();
        StringBuilder table = new StringBuilder();
        for (Map<String, String> user: users) {
            if (!user.get("id").equals(id)) {
                continue;
            }
            table.append("<html><head>"
                    + BOOTSTRAP_STRING
                    + "</head><body><table border=1>");
            response.setLocale(Locale.ROOT);
            table.append("<tr><td>");
            table.append(user.get("id"));
            table.append("</td><td>");
            table.append(user.get("firstName"));
            table.append(" ");
            table.append(user.get("lastName"));
            table.append("</td>");
            table.append("<td>");
            table.append(user.get("email"));
            table.append("</td>");
            table.append("</tr>");
            table.append("</table></body></html>");
            PrintWriter pw = response.getWriter();
            pw.write(table.toString());
            pw.close();
            return;
        }
        response.sendError(404, "Not found");
        // END
    }
}

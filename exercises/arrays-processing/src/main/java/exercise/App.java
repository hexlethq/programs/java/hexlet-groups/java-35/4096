package exercise;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] arr) {

        if(arr == null || arr.length == 0) return -1;

        int result = -1;
        for (int i = 0; i < arr.length; i++) {
            int currentNumber = arr[i];
            if (currentNumber > 0) {
                continue;
            }
            if (result == -1) {
                result = i;
                continue;
            }
            if (arr[result] < arr[i]) {
                result = i;
            }
        }
        return  result;
    }

    public static int[] getElementsLessAverage(int[] arr) {

        if (arr == null || arr.length == 0) return new int[0];

        double average = 0;
        double sum = 0;
        for (int el: arr) {
            sum += el;
        }
        average = sum / arr.length;

        int resultSize = 0;
        for (int el: arr) {
            if (el <= average) resultSize++;
        }
        int[] result = new int[resultSize];
        int i = 0;
        for (int el: arr) {
            if (el <= average) {
                result[i++] = el;
            }
        }
        return result;

    }

    public static int getSumBeforeMinAndMax(int[] arr) {

        if (arr == null || arr.length == 0) return 0;

        int minIndex = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < arr[minIndex]) minIndex = i;
        }

        int maxIndex = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > arr[maxIndex]) maxIndex = i;
        }

        int maxCounter = Math.max(minIndex, maxIndex);
        int sum = 0;
        for (int i = Math.min(minIndex, maxIndex) + 1; i < maxCounter; i++) {
            sum += arr[i];
        }
        return sum;
    }
    // END
}

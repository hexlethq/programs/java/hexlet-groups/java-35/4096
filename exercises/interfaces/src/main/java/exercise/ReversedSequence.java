package exercise;

import java.util.stream.IntStream;

// BEGIN
public class ReversedSequence implements CharSequence {

    private char[] chars;

    public ReversedSequence(String str) {
        int l = str.length();
        chars = new char[l];
        for (; l > 0; l--) {
            chars[str.length() - l] = str.charAt(l - 1);
        }
    }

    @Override
    public int length() {
        return chars.length;
    }

    @Override
    public char charAt(int index) {
        return chars[index - 1];
    }

    @Override
    public boolean isEmpty() {
        return CharSequence.super.isEmpty();
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        int l = end - start;
        char[] subSeq = new char[l];
        for (int i = start; i < end; i++) {
            subSeq[i - start] = chars[i];
        }
        return new String(subSeq);
    }

    @Override
    public IntStream chars() {
        return CharSequence.super.chars();
    }

    @Override
    public IntStream codePoints() {
        return CharSequence.super.codePoints();
    }

    @Override
    public String toString() {
        return String.valueOf(chars);
    }
}
// END

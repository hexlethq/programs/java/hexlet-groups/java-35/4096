package exercise;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class App {
    public static List<String> buildAppartmentsList(List<Home> appartments, int n) {
        Collections.sort(appartments);
        return appartments.stream().limit(n).map(x->x.toString()).collect(Collectors.toList());
    }
}
// END

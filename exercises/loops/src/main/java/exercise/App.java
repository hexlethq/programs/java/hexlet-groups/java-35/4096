package exercise;

import java.util.Locale;

class App {
    // BEGIN
    public static String getAbbreviation(String text) {
        if (text == null) return "";
        String[] words = text.trim().split("[\\p{javaWhitespace}]+");
        StringBuilder result = new StringBuilder();
        for (String word: words) {
            result.append(word.substring(0, 1).toUpperCase());
        }
        return result.toString();
    }
    // END
}

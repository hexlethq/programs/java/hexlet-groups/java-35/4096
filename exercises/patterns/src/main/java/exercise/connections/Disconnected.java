package exercise.connections;

import exercise.TcpConnection;

// BEGIN
public class Disconnected implements Connection{
    TcpConnection tcpConnection;

    public Disconnected(TcpConnection tcpConnection) {
        this.tcpConnection = tcpConnection;
    }

    @Override
    public void connect() {
        tcpConnection.setCurrentState(new Connected(tcpConnection));
    }

    @Override
    public void disconnect() {
        System.out.println("Error. Try to disconnect when connection disconnected.");
    }

}
// END

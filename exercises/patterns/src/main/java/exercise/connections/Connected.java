package exercise.connections;

import exercise.TcpConnection;

// BEGIN
public class Connected implements Connection{

    TcpConnection tcpConnection;

    public Connected(TcpConnection tcpConnection) {
        this.tcpConnection = tcpConnection;
    }

    @Override
    public void connect() {
        System.out.println("Error. Try to connect when connection already established.");
    }

    @Override
    public void disconnect() {
        tcpConnection.setCurrentState(new Disconnected(tcpConnection));
    }

}
// END

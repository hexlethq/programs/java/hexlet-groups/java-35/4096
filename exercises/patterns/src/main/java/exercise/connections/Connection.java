package exercise.connections;

import exercise.TcpConnection;

public interface Connection {
    // BEGIN
    public void connect();
    public void disconnect();
    // END
}

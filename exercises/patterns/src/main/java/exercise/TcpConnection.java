package exercise;
import exercise.connections.Connected;
import exercise.connections.Connection;
import exercise.connections.Disconnected;

import java.util.List;
import java.util.ArrayList;

// BEGIN
public class TcpConnection {
    private Connection currentState;
    private String ip;
    private int port;
    private String message = "";

    public void setCurrentState(Connection currentState) {
        this.currentState = currentState;
    }

    public String getCurrentState() {
        if (currentState instanceof Connected) {
            return "connected";
        } else {
            return "disconnected";
        }
    }

    public TcpConnection(String ip, int port) {
        this.ip = ip;
        this.port = port;
        this.currentState = new Disconnected(this);
    }

    public void connect() {
        currentState.connect();
    }

    public void disconnect() {
        currentState.disconnect();
    }

    public void write(String str) {
        if (currentState instanceof Disconnected) {
            System.out.println("Error. Try to write to disconnected connection.");
            return;
        }
        message = message + str;
    }

}
// END

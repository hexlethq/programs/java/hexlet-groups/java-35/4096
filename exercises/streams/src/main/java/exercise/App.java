package exercise;

import java.util.List;
import java.util.Arrays;

// BEGIN
public class App{
    public static int getCountOfFreeEmails(List<String> emails) {
        String[] freeDomainsArray = {
                "gmail.com",
                "yandex.ru",
                "hotmail.com"
        };
        List<String> freeDomains = Arrays.asList(freeDomainsArray);
        return (int) emails.stream()
                .filter((x) -> (freeDomains.contains(x.substring(x.indexOf("@") + 1))))
                .count();
    }


}
// END

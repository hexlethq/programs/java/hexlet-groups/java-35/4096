package exercise;

import java.util.stream.Collectors;
import java.util.Arrays;

// BEGIN
public class App {
    public static String getForwardedVariables(String inputStr) {
        String environmentStr = "environment=\"";
        String xForwardedStr = "X_FORWARDED_";
        return inputStr.lines()
                .filter(x -> x.startsWith(environmentStr))
                .map(x -> x.substring(environmentStr.length(), x.length()-1))
                .flatMap(x -> (Arrays.stream(x.split(","))))
                .filter(x -> x.startsWith(xForwardedStr))
                .map(x -> x.substring(xForwardedStr.length()))
                .collect(Collectors.joining(","));
    }
}
//END

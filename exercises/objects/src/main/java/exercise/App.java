package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Arrays;
import java.util.Locale;

class App {
    // BEGIN
    public static String buildList(String[] list) {
        if (list.length == 0) return "";
        var res = new StringBuilder("<ul>\n");
        for (String word: list) {
            res.append("  <li>");
            res.append(word);
            res.append("</li>\n");
        }
        res.append("</ul>");
        return res.toString();
    }

    public static String getUsersByYear(String[][] users, int year) {
        if (users.length == 0) return "";
        var res = new StringBuilder();
        for (String[] user: users) {
            if (LocalDate.parse(user[1]).getYear() != year) continue;
            res.append("  <li>");
            res.append(user[0]);
            res.append("</li>\n");
        }
        if (!res.toString().equals("")) {
            res.append("</ul>");
            return  "<ul>\n" + res.toString();
        } else
            return "";
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        if (users.length == 0) return "";
        DateTimeFormatter dTF =
                new DateTimeFormatterBuilder().parseCaseInsensitive()
                        .appendPattern("dd MMM yyyy")
                        .toFormatter(Locale.ENGLISH);
        var localDate = LocalDate.parse(date, dTF);
        String[] minUser = null;
        for (String[] user: users) {
            var userDate  = LocalDate.parse(user[1]);
            if ( !userDate.isBefore(localDate)) continue;
            if (minUser == null) {
                minUser = user;
            } else {
                LocalDate minUserDate = LocalDate.parse(minUser[1]);
                if (userDate.isAfter(minUserDate)) minUser = user;
            }
        }
        if (minUser == null) return "";
        return minUser[0];
        // END
    }
}


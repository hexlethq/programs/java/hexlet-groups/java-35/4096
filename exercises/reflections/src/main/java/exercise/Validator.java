package exercise;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

// BEGIN
public class Validator {
    public static List<String> validate(Object object) {
        var OClass = object.getClass();
        return Arrays.stream(OClass.getDeclaredFields()).
                filter(x -> {
                    try {
                        x.setAccessible(true);
                        return
                        x.getAnnotation(NotNull.class) != null
                                && x.get(object) == null;
                    }
                    catch (Exception e)
                    {
                        System.out.println(e);
                        return false;
                    }
                }).
                map(x -> x.getName()).
                collect(Collectors.toList());

    }

    public static Map<String, List<String>> advancedValidate(Object object) {
        var OClass = object.getClass();
        var stream = Arrays.stream(OClass.getDeclaredFields());
        List<String> nullFields =  stream.
                filter(x -> {
                    try {
                        x.setAccessible(true);
                        return
                                x.getAnnotation(NotNull.class) != null
                                        && x.get(object) == null;
                    }
                    catch (Exception e)
                    {
                        System.out.println(e);
                        return false;
                    }
                }).
                map(x -> x.getName()).
                collect(Collectors.toList());
        stream = Arrays.stream(OClass.getDeclaredFields());
        List<String> minLengthFields =  stream.
                filter(x -> {
                    try {
                        x.setAccessible(true);
                        var ann = x.getAnnotation(MinLength.class);
                        if (ann == null) return false;
                        return ((String) x.get(object)).length() < ann.minLength();
                    }
                    catch (Exception e)
                    {
                        System.out.println(e);
                        return false;
                    }
                }).
                map(x -> x.getName()).
                collect(Collectors.toList());
        Map<String, List<String>> result = new HashMap<>();
        for (String field: minLengthFields) {
            List<String> message = List.of("length less than 4");
            result.put(field, message);
        }
        for (String field: nullFields) {
            List<String> message = result.get(field);
            if (message == null) {
                message = List.of("can not be null");
            } else {
                message.add("can not be null");
            }
            result.put(field, message);
        }

        return result;
    }
}
// END

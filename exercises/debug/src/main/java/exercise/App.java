package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int x, int y, int z) {
        if (x + y < z || x + z < y || y + z < x) {
            return "Треугольник не существует";
        }
        if (x == y && y == z) {
            return "Равносторонний";
        }
        if (x == y || x == z || y == z) {
            return "Равнобедренный";
        }
        return "Разносторонний";
    }

    public static int getFinalGrade(int exam, int project) {
        if (exam > 90 || project > 10)
            return 100;
        if (exam > 75 && project >= 5)
            return 90;
        if (exam > 50 && project >= 2)
            return 75;
        return 0;
    }
    // END
}

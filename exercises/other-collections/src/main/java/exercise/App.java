package exercise;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class App {
    public static Map<String, String> genDiff(Map<String, Object> map1, Map<String, Object> map2) {

        Map<String, String> result = new LinkedHashMap<>();
        Set<String> keySet = new TreeSet<>();
        keySet.addAll(map1.keySet());
        keySet.addAll(map2.keySet());

        for (String el : keySet) {

            Object value1 = map1.get(el);
            Object value2 = map2.get(el);

            if (value1 == null && value2 != null) {
                result.put(el, "added");
            } else if (value1 != null && value2 == null) {
                result.put(el, "deleted");
            } else if (value1.equals(value2)) {
                result.put(el, "unchanged");
            } else {
                result.put(el, "changed");
            }
        }
        return result;
    }
}
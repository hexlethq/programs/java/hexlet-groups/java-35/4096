package exercise;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import io.javalin.Javalin;
import io.ebean.DB;
import io.ebean.Transaction;

import exercise.domain.User;
import exercise.domain.query.QUser;

class AppTest {

    private static Javalin app;
    private static String baseUrl;
    private static Transaction transaction;

    // BEGIN
    @BeforeAll
    public static void beforeAll() {
        // Получаем инстанс приложения
        app = App.getApp();
        // Запускаем приложение на рандомном порту
        app.start(0);
        // Получаем порт, на которм запустилось приложение
        int port = app.port();
        // Формируем базовый URL
        baseUrl = "http://localhost:" + port;
    }

    @AfterAll
    public static void afterAll() {
        // Останавливаем приложение
        app.stop();
    }
    // END

    // Хорошей практикой является запуск тестов с базой данных внутри транзакции.
    // Перед каждым тестом транзакция открывается,
    @BeforeEach
    void beforeEach() {
        transaction = DB.beginTransaction();
    }

    // А после окончания каждого теста транзакция откатывается
    // Таким образом после каждого теста база данных возвращается в исходное состояние,
    // каким оно было перед началом транзакции.
    // Благодаря этому тесты не влияют друг на друга
    @AfterEach
    void afterEach() {
        transaction.rollback();
    }

    @Test
    void testRoot() {
        HttpResponse<String> response = Unirest.get(baseUrl).asString();
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    void testUsers() {

        // Выполняем GET запрос на адрес http://localhost:port/users
        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users")
            .asString();
        // Получаем тело ответа
        String content = response.getBody();

        // Проверяем код ответа
        assertThat(response.getStatus()).isEqualTo(200);
        // Проверяем, что на станице есть определенный текст
        assertThat(content).contains("Wendell Legros");
        assertThat(content).contains("Larry Powlowski");
    }

    @Test
    void testUser() {

        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/5")
            .asString();
        String content = response.getBody();

        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(content).contains("Rolando Larson");
        assertThat(content).contains("galen.hickle@yahoo.com");
    }

    @Test
    void testNewUser() {

        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/new")
            .asString();

        assertThat(response.getStatus()).isEqualTo(200);
    }

    // BEGIN
    @Test
    void testUserValidatorCreateUser() {

        HttpResponse<String> responsePost = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "firstName")
                .field("lastName", "lastName")
                .field("email", "mail@mail.ru")
                .field("password", "password")
                .asString();

        assertThat(responsePost.getStatus()).isEqualTo(302);

        User user = new QUser()
                .firstName.equalTo("firstName")
                .lastName.equalTo("lastName")
                .email.equalTo("mail@mail.ru")
                .password.equalTo("password")
                .findOne();

        assertThat(user).isNotNull();

        assertThat(user.getFirstName()).isEqualTo("firstName");
        assertThat(user.getLastName()).isEqualTo("lastName");
        assertThat(user.getEmail()).isEqualTo("mail@mail.ru");
        assertThat(user.getPassword()).isEqualTo("password");
    }

    @Test
    void testUserValidatorBadMail() {

        HttpResponse<String> responsePost = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "firstName2")
                .field("lastName", "lastName")
                .field("email", "mail.ru")
                .field("password", "password")
                .asString();

        assertThat(responsePost.getStatus()).isEqualTo(422);

        User user = new QUser()
                .firstName.equalTo("firstName2")
                .lastName.equalTo("lastName")
                .findOne();

        assertThat(user).isNull();

        String content = responsePost.getBody();

        assertThat(content).contains("Должно быть валидным email");
    }

    @Test
    void testUserValidatorBadPass() {
        HttpResponse<String> responsePost = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "firstName3")
                .field("lastName", "lastName")
                .field("email", "mail@mail.ru")
                .field("password", "23")
                .asString();

        assertThat(responsePost.getStatus()).isEqualTo(422);

        User user = new QUser()
                .firstName.equalTo("firstName3")
                .lastName.equalTo("lastName")
                .findOne();

        assertThat(user).isNull();

        String content = responsePost.getBody();

        assertThat(content).contains("Пароль должен содержать не менее 4 символов");
    }

    @Test
    void testUserValidatorBadLastName() {

        HttpResponse<String> responsePost = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "firstName4")
                .field("lastName", "")
                .field("email", "mail@mail.ru")
                .field("password", "password123")
                .asString();

        assertThat(responsePost.getStatus()).isEqualTo(422);

        User user = new QUser()
                .firstName.equalTo("firstName4")
                .lastName.equalTo("")
                .email.equalTo("mail@mail.ru")
                .password.equalTo("password123")
                .findOne();

        assertThat(user).isNull();

        String content = responsePost.getBody();

        assertThat(content).contains("Фамилия не должна быть пустой");
    }

    @Test
    void testUserValidatorBadFirstName() {
        HttpResponse<String> responsePost = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "")
                .field("lastName", "lastName5")
                .field("email", "mail@mail.ru")
                .field("password", "password123")
                .asString();

        assertThat(responsePost.getStatus()).isEqualTo(422);

        User user = new QUser()
                .firstName.equalTo("")
                .lastName.equalTo("lastName5")
                .email.equalTo("mail@mail.ru")
                .password.equalTo("password123")
                .findOne();

        assertThat(user).isNull();

        String content = responsePost.getBody();

        assertThat(content).contains("Имя не должно быть пустым");

    }
    // END
}

package exercise;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Map.Entry;

// BEGIN
public class App {

    public static List<Map<String, String>> findWhere(List<Map<String, String>> books, Map<String, String> properties) {
        List<Map<String, String>> result = new ArrayList<>();
        for (Map<String, String> book: books) {
            boolean match = true;
            for (Map.Entry<String, String> property: properties.entrySet()) {
                String key = property.getKey();
                String value = property.getValue();
                if (!book.get(key).equals(value)) {
                    match = false;
                }
            }
            if (match) {
                result.add(book);
            }
        }

        return result;
    }

}
//END
